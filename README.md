# Facebook Posts of the COVID-19 Coordination Centre in Macao

我們的研究團隊從 2020 年 1 月起收集了[澳門特別行政區政府新聞局](https://www.facebook.com/macaogcs/)和[新型冠狀病毒感染應變協調中心](https://www.facebook.com/NCV.Macao)所發佈的 Facebook 消息，當中記錄了超過一年間澳門新冠疫情的發展、政府的應對方案，以及居民大眾的反應。我們的研究顯示，政府社交媒體的參與度與危機階段存在相關性，其中在預警期參與較低，爆發期激增，緩解期逐漸減少；急性期過後，公眾關注的焦點由疫情轉向復蘇等。這些發現對政府社交媒體的應用及其影響具有重要啟示意義。現在我們的研究已進入另一階段，我們亦決定把收集到的原始數據在本網站公開分享，以便其他學者、研究人員以及社會各界人士進行更進一步的分析及研究，並支持研究數據的共享開放的發展。

本數據將不定期進行更新。

如果你使用了我們的數據，我們希望你可以在你的論文或文章中引用我們的原始論文：

Pang PCI, Cai Q, Jiang W, Chan KS. Engagement of Government Social Media on Facebook during the COVID-19 Pandemic in Macao. *International Journal of Environmental Research and Public Health*. 2021; 18(7):3508. https://doi.org/10.3390/ijerph18073508

# 研究團隊

* 彭祥佑，維多利亞大學商學院助理教授
* 陳建新，澳門大學社會科學學院助理教授
* 蔣雯靜，澳門大學社會科學學院碩士生
* 蔡其新，中國人民大學公共管理學院博士生

# 免責聲明

基於研究資源和 Facebook 平台的限制，我們不能確保我們提供的數據絕對完整和正確，使用數據時請考慮這些限制。數據的來源為網上公開的資料，並不代表我們的立場。同時，我們對使用數據所造成的損失或名譽損害不負上任何責任。

# 問題反映或聯絡方式

請使用本網站的 [Issues 功能](https://bitbucket.org/cipang/covid19-facebook-macau/issues?status=new&status=open)去聯絡我們。